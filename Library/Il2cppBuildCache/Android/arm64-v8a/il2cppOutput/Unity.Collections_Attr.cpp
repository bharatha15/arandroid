﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Unity.Collections.BurstCompatibleAttribute
struct BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82;
// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1;
// Unity.Burst.BurstDiscardAttribute
struct BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// Unity.Collections.CreatePropertyAttribute
struct CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E;
// Unity.Collections.NotBurstCompatibleAttribute
struct NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Unity.Burst.BurstCompiler/StaticTypeReinitAttribute
struct StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2;

IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Hash128Long_00000771U24BurstDirectCall_t4D8558DC06E394806F65B9D42F4BE5350663126A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Hash64Long_0000076AU24BurstDirectCall_t51A73249DAE139C68F3876DA569309C36FF3ECA5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_00000733U24BurstDirectCall_t95618C58C9C425D72FE9A3079A99559103D41C14_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_00000A0DU24BurstDirectCall_t6E1AA4032273BF9D9436D204E2DCA4ABB77D108C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_00000A1BU24BurstDirectCall_t661BBFCAA41CAF8D5CF73A67E6E38315B90D41F9_0_0_0_var;

struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// Unity.Collections.BurstCompatibleAttribute
struct BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type[] Unity.Collections.BurstCompatibleAttribute::<GenericTypeArguments>k__BackingField
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___U3CGenericTypeArgumentsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CGenericTypeArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82, ___U3CGenericTypeArgumentsU3Ek__BackingField_0)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_U3CGenericTypeArgumentsU3Ek__BackingField_0() const { return ___U3CGenericTypeArgumentsU3Ek__BackingField_0; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_U3CGenericTypeArgumentsU3Ek__BackingField_0() { return &___U3CGenericTypeArgumentsU3Ek__BackingField_0; }
	inline void set_U3CGenericTypeArgumentsU3Ek__BackingField_0(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___U3CGenericTypeArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGenericTypeArgumentsU3Ek__BackingField_0), (void*)value);
	}
};


// Unity.Burst.BurstDiscardAttribute
struct BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.CreatePropertyAttribute
struct CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Diagnostics.DebuggerTypeProxyAttribute::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeName_0), (void*)value);
	}
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.NotBurstCompatibleAttribute
struct NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Burst.BurstCompiler/StaticTypeReinitAttribute
struct StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Unity.Burst.BurstCompiler/StaticTypeReinitAttribute::reinitType
	Type_t * ___reinitType_0;

public:
	inline static int32_t get_offset_of_reinitType_0() { return static_cast<int32_t>(offsetof(StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2, ___reinitType_0)); }
	inline Type_t * get_reinitType_0() const { return ___reinitType_0; }
	inline Type_t ** get_address_of_reinitType_0() { return &___reinitType_0; }
	inline void set_reinitType_0(Type_t * value)
	{
		___reinitType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reinitType_0), (void*)value);
	}
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CallingConvention
struct CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924 
{
public:
	// System.Int32 System.Runtime.InteropServices.CallingConvention::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CharSet
struct CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B 
{
public:
	// System.Int32 System.Runtime.InteropServices.CharSet::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Burst.FloatMode
struct FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4 
{
public:
	// System.Int32 Unity.Burst.FloatMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Burst.FloatPrecision
struct FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5 
{
public:
	// System.Int32 Unity.Burst.FloatPrecision::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// Unity.Burst.FloatMode Unity.Burst.BurstCompileAttribute::<FloatMode>k__BackingField
	int32_t ___U3CFloatModeU3Ek__BackingField_0;
	// Unity.Burst.FloatPrecision Unity.Burst.BurstCompileAttribute::<FloatPrecision>k__BackingField
	int32_t ___U3CFloatPrecisionU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> Unity.Burst.BurstCompileAttribute::_compileSynchronously
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ____compileSynchronously_2;
	// System.String[] Unity.Burst.BurstCompileAttribute::<Options>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFloatModeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatModeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFloatModeU3Ek__BackingField_0() const { return ___U3CFloatModeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFloatModeU3Ek__BackingField_0() { return &___U3CFloatModeU3Ek__BackingField_0; }
	inline void set_U3CFloatModeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFloatModeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFloatPrecisionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatPrecisionU3Ek__BackingField_1)); }
	inline int32_t get_U3CFloatPrecisionU3Ek__BackingField_1() const { return ___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CFloatPrecisionU3Ek__BackingField_1() { return &___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline void set_U3CFloatPrecisionU3Ek__BackingField_1(int32_t value)
	{
		___U3CFloatPrecisionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__compileSynchronously_2() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ____compileSynchronously_2)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get__compileSynchronously_2() const { return ____compileSynchronously_2; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of__compileSynchronously_2() { return &____compileSynchronously_2; }
	inline void set__compileSynchronously_2(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		____compileSynchronously_2 = value;
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3COptionsU3Ek__BackingField_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COptionsU3Ek__BackingField_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::m_callingConvention
	int32_t ___m_callingConvention_0;
	// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::CharSet
	int32_t ___CharSet_1;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::BestFitMapping
	bool ___BestFitMapping_2;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::ThrowOnUnmappableChar
	bool ___ThrowOnUnmappableChar_3;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::SetLastError
	bool ___SetLastError_4;

public:
	inline static int32_t get_offset_of_m_callingConvention_0() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___m_callingConvention_0)); }
	inline int32_t get_m_callingConvention_0() const { return ___m_callingConvention_0; }
	inline int32_t* get_address_of_m_callingConvention_0() { return &___m_callingConvention_0; }
	inline void set_m_callingConvention_0(int32_t value)
	{
		___m_callingConvention_0 = value;
	}

	inline static int32_t get_offset_of_CharSet_1() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___CharSet_1)); }
	inline int32_t get_CharSet_1() const { return ___CharSet_1; }
	inline int32_t* get_address_of_CharSet_1() { return &___CharSet_1; }
	inline void set_CharSet_1(int32_t value)
	{
		___CharSet_1 = value;
	}

	inline static int32_t get_offset_of_BestFitMapping_2() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___BestFitMapping_2)); }
	inline bool get_BestFitMapping_2() const { return ___BestFitMapping_2; }
	inline bool* get_address_of_BestFitMapping_2() { return &___BestFitMapping_2; }
	inline void set_BestFitMapping_2(bool value)
	{
		___BestFitMapping_2 = value;
	}

	inline static int32_t get_offset_of_ThrowOnUnmappableChar_3() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___ThrowOnUnmappableChar_3)); }
	inline bool get_ThrowOnUnmappableChar_3() const { return ___ThrowOnUnmappableChar_3; }
	inline bool* get_address_of_ThrowOnUnmappableChar_3() { return &___ThrowOnUnmappableChar_3; }
	inline void set_ThrowOnUnmappableChar_3(bool value)
	{
		___ThrowOnUnmappableChar_3 = value;
	}

	inline static int32_t get_offset_of_SetLastError_4() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___SetLastError_4)); }
	inline bool get_SetLastError_4() const { return ___SetLastError_4; }
	inline bool* get_address_of_SetLastError_4() { return &___SetLastError_4; }
	inline void set_SetLastError_4(bool value)
	{
		___SetLastError_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompiler/StaticTypeReinitAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1 (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * __this, Type_t * ___toReinit0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * __this, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstDiscardAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9 (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7 (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * __this, int32_t ___callingConvention0, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompileAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628 (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * __this, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompileAttribute::set_CompileSynchronously(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025 (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344 (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.BurstCompatibleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.BurstCompatibleAttribute::set_GenericTypeArguments(System.Type[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167 (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NotBurstCompatibleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63 (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.CreatePropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68 (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void Unity_Collections_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hash128Long_00000771U24BurstDirectCall_t4D8558DC06E394806F65B9D42F4BE5350663126A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hash64Long_0000076AU24BurstDirectCall_t51A73249DAE139C68F3876DA569309C36FF3ECA5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_00000733U24BurstDirectCall_t95618C58C9C425D72FE9A3079A99559103D41C14_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_00000A0DU24BurstDirectCall_t6E1AA4032273BF9D9436D204E2DCA4ABB77D108C_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_00000A1BU24BurstDirectCall_t661BBFCAA41CAF8D5CF73A67E6E38315B90D41F9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x53\x63\x65\x6E\x65\x73"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[2];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[4];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x42\x75\x72\x73\x74\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x73\x2E\x42\x75\x72\x73\x74\x43\x6F\x6D\x70\x61\x74\x69\x62\x69\x6C\x69\x74\x79\x54\x65\x73\x74\x43\x6F\x64\x65\x47\x65\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x73\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x43\x6F\x64\x65\x47\x65\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x45\x64\x69\x74\x6F\x72\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E\x48\x79\x62\x72\x69\x64"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x53\x63\x65\x6E\x65\x73\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[17];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Hash64Long_0000076AU24BurstDirectCall_t51A73249DAE139C68F3876DA569309C36FF3ECA5_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[18];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_00000733U24BurstDirectCall_t95618C58C9C425D72FE9A3079A99559103D41C14_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[19];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_00000A1BU24BurstDirectCall_t661BBFCAA41CAF8D5CF73A67E6E38315B90D41F9_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[20];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_00000A0DU24BurstDirectCall_t6E1AA4032273BF9D9436D204E2DCA4ABB77D108C_0_0_0_var), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x50\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x6D\x70\x6C\x65\x73\x2E\x47\x72\x69\x64\x50\x61\x74\x68\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x49\x4F\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[24];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Hash128Long_00000771U24BurstDirectCall_t4D8558DC06E394806F65B9D42F4BE5350663126A_0_0_0_var), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x52\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E\x4E\x61\x74\x69\x76\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x47\x61\x6D\x65\x53\x61\x76\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x47\x61\x6D\x65\x53\x61\x76\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x43\x6F\x72\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x55\x6E\x69\x74\x79\x49\x6E\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * tmp = (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D(tmp, NULL);
	}
}
static void IsUnmanagedAttribute_tC3711779D00EFADD8F826DD8391CFD36FC1B912F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * tmp = (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_CheckDelegate_m2034F06B2464A4A27E2B24F536B2F2244CFDC603(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_forward_mono_allocate_block_m490B419D75E5521086A86698341989DD47CBD8B6(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[1];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void Try_00000A0DU24BurstDirectCall_t6E1AA4032273BF9D9436D204E2DCA4ABB77D108C_CustomAttributesCacheGenerator_Try_00000A0DU24BurstDirectCall_GetFunctionPointerDiscard_m84BAF84015A781A693374369955208145DCF1287(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[1];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void Try_00000A1BU24BurstDirectCall_t661BBFCAA41CAF8D5CF73A67E6E38315B90D41F9_CustomAttributesCacheGenerator_Try_00000A1BU24BurstDirectCall_GetFunctionPointerDiscard_mA13A97454F15CEA9E01D5DAE9885D64405BDC160(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 236LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_U3CGenericTypeArgumentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 224LL, NULL);
	}
}
static void CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator_FixedList_PaddingBytes_m5094730BB004E6550AC23A4E96392121DB77FA7C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[0];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_Equals_m3AE49BA608D98AFD99FAE97509B7A341DD7A4B5C(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[0];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_Equals_m370E69052A57C18F0FC4A26AC65588E3E7E5E562(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[2];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var), NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_Equals_m98E0AA2048211F341CA8272EDE35FD28E1A024CD(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[2];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_Equals_m360A4228798FD89819FC89B23A6095312A80C7F2(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_Equals_m32CF680F6D80FB99E1C8659269C9313745A41312(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedBytes16_t6F3DA12A9BFDF36F711B4EE37C752953B2125C37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes30_tCCED5D93977503BC77E394236A7023519785DC80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes62_tC77E73BB842294F4F9A4807C0857B8F332D52A72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes126_t10EF6170BCD75D7F5463F602828B457A3C98EC39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes510_t71D0D7868DFFD20E428EBBB5F8AF60636FD16D8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes4094_tA9F73D0C7311C392EEC2D43FCABB33894D30A150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Memory_tCB9F7958AD19492CF5CC6A72E2B7635AECE2A1E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator_Unmanaged_Free_m8F82A7CFBFD0519251F3AEF8C4B1B536633A2F2F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator_Array_Resize_mE8E09A071494DB398D0B83EE15E49A55F0FFBE32(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnmanagedArray_1_t17817821392C6FC7C52505CDC6F87AE9D52CE526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[1];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void MemoryBlock_tFE475C7AFDA538E4DE9FF1F547E14BD0E7D0C073_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Try_00000733U24BurstDirectCall_t95618C58C9C425D72FE9A3079A99559103D41C14_CustomAttributesCacheGenerator_Try_00000733U24BurstDirectCall_GetFunctionPointerDiscard_m890205B4036748F9AB5D9A0C4FA7CE2C44DEF745(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[2];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[3];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void Hash64Long_0000076AU24BurstDirectCall_t51A73249DAE139C68F3876DA569309C36FF3ECA5_CustomAttributesCacheGenerator_Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointerDiscard_m766542CA26A2A488488CE16DE600528552D22342(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void Hash128Long_00000771U24BurstDirectCall_t4D8558DC06E394806F65B9D42F4BE5350663126A_CustomAttributesCacheGenerator_Hash128Long_00000771U24BurstDirectCall_GetFunctionPointerDiscard_mD8EA149456EACFBE282AA22581E449DFFBBD1CDC(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void U24BurstDirectCallInitializer_tAC3E0907180450DFB40E146971FACAABE51C2F33_CustomAttributesCacheGenerator_U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 2LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[70] = 
{
	EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6_CustomAttributesCacheGenerator,
	IsUnmanagedAttribute_tC3711779D00EFADD8F826DD8391CFD36FC1B912F_CustomAttributesCacheGenerator,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator,
	TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_CustomAttributesCacheGenerator,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator,
	NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163_CustomAttributesCacheGenerator,
	CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator,
	FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator,
	FixedBytes16_t6F3DA12A9BFDF36F711B4EE37C752953B2125C37_CustomAttributesCacheGenerator,
	FixedBytes30_tCCED5D93977503BC77E394236A7023519785DC80_CustomAttributesCacheGenerator,
	FixedBytes62_tC77E73BB842294F4F9A4807C0857B8F332D52A72_CustomAttributesCacheGenerator,
	FixedBytes126_t10EF6170BCD75D7F5463F602828B457A3C98EC39_CustomAttributesCacheGenerator,
	FixedBytes510_t71D0D7868DFFD20E428EBBB5F8AF60636FD16D8F_CustomAttributesCacheGenerator,
	FixedBytes4094_tA9F73D0C7311C392EEC2D43FCABB33894D30A150_CustomAttributesCacheGenerator,
	Memory_tCB9F7958AD19492CF5CC6A72E2B7635AECE2A1E9_CustomAttributesCacheGenerator,
	Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator,
	Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator,
	UnmanagedArray_1_t17817821392C6FC7C52505CDC6F87AE9D52CE526_CustomAttributesCacheGenerator,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator,
	MemoryBlock_tFE475C7AFDA538E4DE9FF1F547E14BD0E7D0C073_CustomAttributesCacheGenerator,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_U3CGenericTypeArgumentsU3Ek__BackingField,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_length,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_buffer,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_length,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_buffer,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_length,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_buffer,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_length,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_buffer,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_length,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_buffer,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_CheckDelegate_m2034F06B2464A4A27E2B24F536B2F2244CFDC603,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_forward_mono_allocate_block_m490B419D75E5521086A86698341989DD47CBD8B6,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B,
	Try_00000A0DU24BurstDirectCall_t6E1AA4032273BF9D9436D204E2DCA4ABB77D108C_CustomAttributesCacheGenerator_Try_00000A0DU24BurstDirectCall_GetFunctionPointerDiscard_m84BAF84015A781A693374369955208145DCF1287,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5,
	Try_00000A1BU24BurstDirectCall_t661BBFCAA41CAF8D5CF73A67E6E38315B90D41F9_CustomAttributesCacheGenerator_Try_00000A1BU24BurstDirectCall_GetFunctionPointerDiscard_mA13A97454F15CEA9E01D5DAE9885D64405BDC160,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A,
	FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator_FixedList_PaddingBytes_m5094730BB004E6550AC23A4E96392121DB77FA7C,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_Equals_m3AE49BA608D98AFD99FAE97509B7A341DD7A4B5C,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_Equals_m370E69052A57C18F0FC4A26AC65588E3E7E5E562,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_Equals_m98E0AA2048211F341CA8272EDE35FD28E1A024CD,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_Equals_m360A4228798FD89819FC89B23A6095312A80C7F2,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_Equals_m32CF680F6D80FB99E1C8659269C9313745A41312,
	Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator_Unmanaged_Free_m8F82A7CFBFD0519251F3AEF8C4B1B536633A2F2F,
	Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator_Array_Resize_mE8E09A071494DB398D0B83EE15E49A55F0FFBE32,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE,
	Try_00000733U24BurstDirectCall_t95618C58C9C425D72FE9A3079A99559103D41C14_CustomAttributesCacheGenerator_Try_00000733U24BurstDirectCall_GetFunctionPointerDiscard_m890205B4036748F9AB5D9A0C4FA7CE2C44DEF745,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D,
	Hash64Long_0000076AU24BurstDirectCall_t51A73249DAE139C68F3876DA569309C36FF3ECA5_CustomAttributesCacheGenerator_Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointerDiscard_m766542CA26A2A488488CE16DE600528552D22342,
	Hash128Long_00000771U24BurstDirectCall_t4D8558DC06E394806F65B9D42F4BE5350663126A_CustomAttributesCacheGenerator_Hash128Long_00000771U24BurstDirectCall_GetFunctionPointerDiscard_mD8EA149456EACFBE282AA22581E449DFFBBD1CDC,
	U24BurstDirectCallInitializer_tAC3E0907180450DFB40E146971FACAABE51C2F33_CustomAttributesCacheGenerator_U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9____Length_PropertyInfo,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52____Length_PropertyInfo,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38____Length_PropertyInfo,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5____Length_PropertyInfo,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8____Length_PropertyInfo,
	Unity_Collections_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___value0, const RuntimeMethod* method)
{
	{
		// public Type[] GenericTypeArguments { get; set; }
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_0 = ___value0;
		__this->set_U3CGenericTypeArgumentsU3Ek__BackingField_0(L_0);
		return;
	}
}
