﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE_RuntimeMethod_var;
extern const RuntimeMethod* RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066_RuntimeMethod_var;
extern const RuntimeMethod* SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5_RuntimeMethod_var;
extern const RuntimeMethod* SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8_RuntimeMethod_var;
extern const RuntimeMethod* StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B_RuntimeMethod_var;
extern const RuntimeMethod* StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B_RuntimeMethod_var;



// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m05BDDBB49F005C47C815CD32668381083A1F5C43 (void);
// 0x00000003 System.Void Unity.Collections.AllocatorManager::CheckDelegate(System.Boolean&)
extern void AllocatorManager_CheckDelegate_m2034F06B2464A4A27E2B24F536B2F2244CFDC603 (void);
// 0x00000004 System.Boolean Unity.Collections.AllocatorManager::UseDelegate()
extern void AllocatorManager_UseDelegate_m502251BCD61C58A4C55C3B51DC6DBC56B2F86981 (void);
// 0x00000005 System.Int32 Unity.Collections.AllocatorManager::allocate_block(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_allocate_block_mF37B0FD92BC1BF98C2A5C765E4F2AB9532300EE7 (void);
// 0x00000006 System.Void Unity.Collections.AllocatorManager::forward_mono_allocate_block(Unity.Collections.AllocatorManager/Block&,System.Int32&)
extern void AllocatorManager_forward_mono_allocate_block_m490B419D75E5521086A86698341989DD47CBD8B6 (void);
// 0x00000007 Unity.Collections.Allocator Unity.Collections.AllocatorManager::LegacyOf(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void AllocatorManager_LegacyOf_m9F8300F5AEED87F3B9FCD28C81A52B53D58B697B (void);
// 0x00000008 System.Int32 Unity.Collections.AllocatorManager::TryLegacy(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_TryLegacy_m7086EEBC033BDCAB24CAC49D695180EF1BA46895 (void);
// 0x00000009 System.Int32 Unity.Collections.AllocatorManager::Try(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_Try_m67E1175F6B4340CF45D552DDC90CDFE0DA1912E3 (void);
// 0x0000000A System.Void Unity.Collections.AllocatorManager::.cctor()
extern void AllocatorManager__cctor_m704686DC15590B3752449701AC79493E5DBB4681 (void);
// 0x0000000B System.Void Unity.Collections.AllocatorManager/TryFunction::.ctor(System.Object,System.IntPtr)
extern void TryFunction__ctor_m61B0A7506F9D0C752E09C91E3E3CC16034A41AA0 (void);
// 0x0000000C System.Int32 Unity.Collections.AllocatorManager/TryFunction::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void TryFunction_Invoke_m62DB13101BCEC040485DBD4F68E9B4B9406368DE (void);
// 0x0000000D System.IAsyncResult Unity.Collections.AllocatorManager/TryFunction::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void TryFunction_BeginInvoke_mDB5760D98471188127E578B6598BBD7D498ACCF2 (void);
// 0x0000000E System.Int32 Unity.Collections.AllocatorManager/TryFunction::EndInvoke(Unity.Collections.AllocatorManager/Block&,System.IAsyncResult)
extern void TryFunction_EndInvoke_m26A14616127F9673336FE6BB8C1202F6A7F27C44 (void);
// 0x0000000F Unity.Collections.AllocatorManager/TableEntry& Unity.Collections.AllocatorManager/AllocatorHandle::get_TableEntry()
extern void AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D (void);
// 0x00000010 System.Void Unity.Collections.AllocatorManager/AllocatorHandle::Rewind()
extern void AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060 (void);
// 0x00000011 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/AllocatorHandle::op_Implicit(Unity.Collections.Allocator)
extern void AllocatorHandle_op_Implicit_m63D8E96033A00071E8FDEC80D6956ADBE627067C (void);
// 0x00000012 System.Int32 Unity.Collections.AllocatorManager/AllocatorHandle::get_Value()
extern void AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B (void);
// 0x00000013 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/AllocatorHandle::get_Handle()
extern void AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E (void);
// 0x00000014 Unity.Collections.Allocator Unity.Collections.AllocatorManager/AllocatorHandle::get_ToAllocator()
extern void AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF (void);
// 0x00000015 System.Void Unity.Collections.AllocatorManager/AllocatorHandle::Dispose()
extern void AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1 (void);
// 0x00000016 System.Void Unity.Collections.AllocatorManager/Range::Dispose()
extern void Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42 (void);
// 0x00000017 System.Int64 Unity.Collections.AllocatorManager/Block::get_Bytes()
extern void Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA (void);
// 0x00000018 System.Int64 Unity.Collections.AllocatorManager/Block::get_AllocatedBytes()
extern void Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907 (void);
// 0x00000019 System.Int32 Unity.Collections.AllocatorManager/Block::get_Alignment()
extern void Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF (void);
// 0x0000001A System.Void Unity.Collections.AllocatorManager/Block::set_Alignment(System.Int32)
extern void Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730 (void);
// 0x0000001B System.Void Unity.Collections.AllocatorManager/Block::Dispose()
extern void Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9 (void);
// 0x0000001C System.Int32 Unity.Collections.AllocatorManager/Block::TryFree()
extern void Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1 (void);
// 0x0000001D System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D (void);
// 0x0000001E System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B (void);
// 0x0000001F System.Void Unity.Collections.AllocatorManager/StackAllocator::Dispose()
extern void StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678 (void);
// 0x00000020 System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B (void);
// 0x00000021 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_00000A0DU24PostfixBurstDelegate__ctor_m6CA496ED55E8C0A79AF1416EACA73B7DC105A1E8 (void);
// 0x00000022 System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000A0DU24PostfixBurstDelegate_Invoke_m14771936EA82A88235522B9505C4ED11040C3E12 (void);
// 0x00000023 System.IAsyncResult Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_00000A0DU24PostfixBurstDelegate_BeginInvoke_mB5F366704B329E5C7ACC3AA5EC77C546DE0975DB (void);
// 0x00000024 System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_00000A0DU24PostfixBurstDelegate_EndInvoke_mDFD46823A5E6964B98E6941C9E19CE53D792EA91 (void);
// 0x00000025 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_00000A0DU24BurstDirectCall_GetFunctionPointerDiscard_m84BAF84015A781A693374369955208145DCF1287 (void);
// 0x00000026 System.IntPtr Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::GetFunctionPointer()
extern void Try_00000A0DU24BurstDirectCall_GetFunctionPointer_m5105FA31F2C62F8E25F93557CCBCDBDB8097B304 (void);
// 0x00000027 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::Constructor()
extern void Try_00000A0DU24BurstDirectCall_Constructor_mC9E73E28C439C59971AB2CCFD142B3C545DA351D (void);
// 0x00000028 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::Initialize()
extern void Try_00000A0DU24BurstDirectCall_Initialize_mAA683C51A2EBF891DB5B0979D334D683FDB2AA7B (void);
// 0x00000029 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::.cctor()
extern void Try_00000A0DU24BurstDirectCall__cctor_m23A5E5C3C2CB347DB2BF1C9F1BA78FCEF0A667D3 (void);
// 0x0000002A System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_00000A0D$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000A0DU24BurstDirectCall_Invoke_m5E38DA0377AC51E89FC0B77A91DA26BB95820C48 (void);
// 0x0000002B System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::get_SlabSizeInBytes()
extern void SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685 (void);
// 0x0000002C System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3 (void);
// 0x0000002D System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8 (void);
// 0x0000002E System.Void Unity.Collections.AllocatorManager/SlabAllocator::Dispose()
extern void SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F (void);
// 0x0000002F System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5 (void);
// 0x00000030 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_00000A1BU24PostfixBurstDelegate__ctor_mD55E80D851C62D0199833375E332FB8568804DE3 (void);
// 0x00000031 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000A1BU24PostfixBurstDelegate_Invoke_mC5B849FCF13EC1E9902C5F88F7C0E467F1676E45 (void);
// 0x00000032 System.IAsyncResult Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_00000A1BU24PostfixBurstDelegate_BeginInvoke_m417507BF7BA1FF01C7E671A652BCE33B2785E1F5 (void);
// 0x00000033 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_00000A1BU24PostfixBurstDelegate_EndInvoke_mA5EFF693AF5DA68E98F99AB057ADC0681BBF3544 (void);
// 0x00000034 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_00000A1BU24BurstDirectCall_GetFunctionPointerDiscard_mA13A97454F15CEA9E01D5DAE9885D64405BDC160 (void);
// 0x00000035 System.IntPtr Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::GetFunctionPointer()
extern void Try_00000A1BU24BurstDirectCall_GetFunctionPointer_m55DFEEEAD2C4157915074EB6B0CFA639BD4DE29F (void);
// 0x00000036 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::Constructor()
extern void Try_00000A1BU24BurstDirectCall_Constructor_m082EAA29ABF433DE3AE1809D8FEF35798D9AC630 (void);
// 0x00000037 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::Initialize()
extern void Try_00000A1BU24BurstDirectCall_Initialize_m6CA9079BFC71C764220C98C5F1B2D4E3117F5891 (void);
// 0x00000038 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::.cctor()
extern void Try_00000A1BU24BurstDirectCall__cctor_m437D4396F3B6ABD61A66D354825A25B1908576AE (void);
// 0x00000039 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_00000A1B$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000A1BU24BurstDirectCall_Invoke_m365F55D30BBADEEB5CBC42E9E010BF66363886F0 (void);
// 0x0000003A T& Unity.Collections.AllocatorManager/Array32768`1::ElementAt(System.Int32)
// 0x0000003B System.Void Unity.Collections.AllocatorManager/SharedStatics/TableEntry::.cctor()
extern void TableEntry__cctor_m4E930B1CD3F5349C2EE468379393D62B7F40908F (void);
// 0x0000003C System.Void Unity.Collections.AllocatorManager/Managed::.cctor()
extern void Managed__cctor_mDB5004E87DDAA2A1039890601B936875DECCD4EE (void);
// 0x0000003D System.Void Unity.Collections.CreatePropertyAttribute::.ctor()
extern void CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68 (void);
// 0x0000003E System.Void Unity.Collections.BurstCompatibleAttribute::set_GenericTypeArguments(System.Type[])
extern void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A (void);
// 0x0000003F System.Void Unity.Collections.BurstCompatibleAttribute::.ctor()
extern void BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C (void);
// 0x00000040 System.Void Unity.Collections.NotBurstCompatibleAttribute::.ctor()
extern void NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63 (void);
// 0x00000041 System.UInt32 Unity.Collections.CollectionHelper::Hash(System.Void*,System.Int32)
extern void CollectionHelper_Hash_m8132774EA770BB87BF148F3B4900F64CCA47F576 (void);
// 0x00000042 System.Int32 Unity.Collections.CollectionHelper::AssumePositive(System.Int32)
extern void CollectionHelper_AssumePositive_mD8785D4C9E69993993EA9982A5F6F2ADAFE5B110 (void);
// 0x00000043 System.Int32 Unity.Collections.FixedList::PaddingBytes()
// 0x00000044 System.Int32 Unity.Collections.FixedList32Bytes`1::get_Length()
// 0x00000045 System.Int32 Unity.Collections.FixedList32Bytes`1::get_LengthInBytes()
// 0x00000046 System.Byte* Unity.Collections.FixedList32Bytes`1::get_Buffer()
// 0x00000047 System.Int32 Unity.Collections.FixedList32Bytes`1::GetHashCode()
// 0x00000048 System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x00000049 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000004A System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000004B System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000004C System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000004D System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000004E System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x0000004F System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000050 System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000051 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000052 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(System.Object)
// 0x00000053 System.Collections.IEnumerator Unity.Collections.FixedList32Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList32Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000055 System.Int32 Unity.Collections.FixedList64Bytes`1::get_Length()
// 0x00000056 System.Int32 Unity.Collections.FixedList64Bytes`1::get_LengthInBytes()
// 0x00000057 System.Byte* Unity.Collections.FixedList64Bytes`1::get_Buffer()
// 0x00000058 System.Int32 Unity.Collections.FixedList64Bytes`1::GetHashCode()
// 0x00000059 System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000005A System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000005B System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000005C System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000005D System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000005E System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000005F System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000060 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000061 System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000062 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000063 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(System.Object)
// 0x00000064 System.Collections.IEnumerator Unity.Collections.FixedList64Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000065 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList64Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000066 System.Int32 Unity.Collections.FixedList128Bytes`1::get_Length()
// 0x00000067 System.Int32 Unity.Collections.FixedList128Bytes`1::get_LengthInBytes()
// 0x00000068 System.Byte* Unity.Collections.FixedList128Bytes`1::get_Buffer()
// 0x00000069 System.Int32 Unity.Collections.FixedList128Bytes`1::GetHashCode()
// 0x0000006A System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000006B System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000006C System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000006D System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000006E System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000006F System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000070 System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000071 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000072 System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000073 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000074 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(System.Object)
// 0x00000075 System.Collections.IEnumerator Unity.Collections.FixedList128Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000076 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList128Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000077 System.Int32 Unity.Collections.FixedList512Bytes`1::get_Length()
// 0x00000078 System.Int32 Unity.Collections.FixedList512Bytes`1::get_LengthInBytes()
// 0x00000079 System.Byte* Unity.Collections.FixedList512Bytes`1::get_Buffer()
// 0x0000007A System.Int32 Unity.Collections.FixedList512Bytes`1::GetHashCode()
// 0x0000007B System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000007C System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000007D System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000007E System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000007F System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000080 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000081 System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000082 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000083 System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000084 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000085 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(System.Object)
// 0x00000086 System.Collections.IEnumerator Unity.Collections.FixedList512Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000087 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList512Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000088 System.Int32 Unity.Collections.FixedList4096Bytes`1::get_Length()
// 0x00000089 System.Int32 Unity.Collections.FixedList4096Bytes`1::get_LengthInBytes()
// 0x0000008A System.Byte* Unity.Collections.FixedList4096Bytes`1::get_Buffer()
// 0x0000008B T Unity.Collections.FixedList4096Bytes`1::get_Item(System.Int32)
// 0x0000008C System.Void Unity.Collections.FixedList4096Bytes`1::set_Item(System.Int32,T)
// 0x0000008D System.Int32 Unity.Collections.FixedList4096Bytes`1::GetHashCode()
// 0x0000008E System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000008F System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x00000090 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x00000091 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x00000092 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000093 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000094 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000095 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000096 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000097 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000098 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(System.Object)
// 0x00000099 System.Collections.IEnumerator Unity.Collections.FixedList4096Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList4096Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000009B System.Void* Unity.Collections.Memory/Unmanaged::Allocate(System.Int64,System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Unmanaged_Allocate_mE42F0479C571BE76773614A408329CFCB51FB7F5 (void);
// 0x0000009C System.Void Unity.Collections.Memory/Unmanaged::Free(System.Void*,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Unmanaged_Free_mE141FBBD8FC68C23121409BD2187EDDAB7849045 (void);
// 0x0000009D System.Void Unity.Collections.Memory/Unmanaged::Free(T*,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x0000009E System.Boolean Unity.Collections.Memory/Unmanaged/Array::IsCustom(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Array_IsCustom_m5553247492EBFB5ECECF39E326F1EC5C3F7FA892 (void);
// 0x0000009F System.Void* Unity.Collections.Memory/Unmanaged/Array::CustomResize(System.Void*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int64,System.Int32)
extern void Array_CustomResize_mD9B4173A8E9495BECF4359DE1B09E72428F2C521 (void);
// 0x000000A0 System.Void* Unity.Collections.Memory/Unmanaged/Array::Resize(System.Void*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int64,System.Int32)
extern void Array_Resize_m7334DEAC65445477E1D278AC2DFEBD53C06C2C1B (void);
// 0x000000A1 T* Unity.Collections.Memory/Unmanaged/Array::Resize(T*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000A2 System.Void Unity.Collections.Spinner::Lock()
extern void Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1 (void);
// 0x000000A3 System.Void Unity.Collections.Spinner::Unlock()
extern void Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32 (void);
// 0x000000A4 System.Void Unity.Collections.UnmanagedArray`1::Dispose()
// 0x000000A5 T& Unity.Collections.UnmanagedArray`1::get_Item(System.Int32)
// 0x000000A6 System.Void Unity.Collections.RewindableAllocator::Rewind()
extern void RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580 (void);
// 0x000000A7 System.Void Unity.Collections.RewindableAllocator::Dispose()
extern void RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678 (void);
// 0x000000A8 System.Int32 Unity.Collections.RewindableAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE (void);
// 0x000000A9 System.Int32 Unity.Collections.RewindableAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066 (void);
// 0x000000AA System.Int32 Unity.Collections.RewindableAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE (void);
// 0x000000AB System.Void Unity.Collections.RewindableAllocator/MemoryBlock::.ctor(System.Int64)
extern void MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3 (void);
// 0x000000AC System.Void Unity.Collections.RewindableAllocator/MemoryBlock::Rewind()
extern void MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958 (void);
// 0x000000AD System.Void Unity.Collections.RewindableAllocator/MemoryBlock::Dispose()
extern void MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF (void);
// 0x000000AE System.Int32 Unity.Collections.RewindableAllocator/MemoryBlock::TryAllocate(Unity.Collections.AllocatorManager/Block&)
extern void MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F (void);
// 0x000000AF System.Boolean Unity.Collections.RewindableAllocator/MemoryBlock::Contains(System.IntPtr)
extern void MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB (void);
// 0x000000B0 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_00000733U24PostfixBurstDelegate__ctor_mC269FA37E2C581177FDAB83EBAD30A32DAC72195 (void);
// 0x000000B1 System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000733U24PostfixBurstDelegate_Invoke_m557F3B6E7AA123BE278B8A696F52F1C131428479 (void);
// 0x000000B2 System.IAsyncResult Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_00000733U24PostfixBurstDelegate_BeginInvoke_m90138D17DDDBF0F9E33B6F487300B6F4809965AE (void);
// 0x000000B3 System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_00000733U24PostfixBurstDelegate_EndInvoke_m66C4B08A34BC81B9C7672D23353625FFF0B6C537 (void);
// 0x000000B4 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_00000733U24BurstDirectCall_GetFunctionPointerDiscard_m890205B4036748F9AB5D9A0C4FA7CE2C44DEF745 (void);
// 0x000000B5 System.IntPtr Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::GetFunctionPointer()
extern void Try_00000733U24BurstDirectCall_GetFunctionPointer_m06ECE770F48FFF3B3D94FCA18C32F79DCB28B459 (void);
// 0x000000B6 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::Constructor()
extern void Try_00000733U24BurstDirectCall_Constructor_mA2CE9EEA9180483974A6EC4D6A9D36DB03734680 (void);
// 0x000000B7 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::Initialize()
extern void Try_00000733U24BurstDirectCall_Initialize_m8F25AF68D2CB64DA669895C921739A9CF99A9ECE (void);
// 0x000000B8 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::.cctor()
extern void Try_00000733U24BurstDirectCall__cctor_m3511B05AC0CB0C1AB71F4E4224B3E082991DE4DD (void);
// 0x000000B9 System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_00000733$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_00000733U24BurstDirectCall_Invoke_m4FEDD13AEB6796E178E5255555EB79DF67E91FE2 (void);
// 0x000000BA System.Void Unity.Collections.xxHash3::Avx2HashLongInternalLoop(System.UInt64*,System.Byte*,System.Byte*,System.Int64,System.Byte*,System.Int32)
extern void xxHash3_Avx2HashLongInternalLoop_mCCB0DF801328F7B4907CCD550BA0E9C2A971E4AF (void);
// 0x000000BB System.Void Unity.Collections.xxHash3::Avx2ScrambleAcc(System.UInt64*,System.Byte*)
extern void xxHash3_Avx2ScrambleAcc_mA6DBBF7E016635A901F80FC07FF1851618D6C3D5 (void);
// 0x000000BC System.Void Unity.Collections.xxHash3::Avx2Accumulate(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int64,System.Int32)
extern void xxHash3_Avx2Accumulate_m140D43D5123949B4A98F707CE310C744F78BC35B (void);
// 0x000000BD System.Void Unity.Collections.xxHash3::Avx2Accumulate512(System.UInt64*,System.Byte*,System.Byte*,System.Byte*)
extern void xxHash3_Avx2Accumulate512_m7C23025DBFD191FE18600B12063730E6739850C7 (void);
// 0x000000BE System.UInt64 Unity.Collections.xxHash3::Hash64Long(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7 (void);
// 0x000000BF System.Void Unity.Collections.xxHash3::Hash128Long(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D (void);
// 0x000000C0 Unity.Mathematics.uint4 Unity.Collections.xxHash3::ToUint4(System.UInt64,System.UInt64)
extern void xxHash3_ToUint4_m8417C90298E7430A8DCD51069984F8AE71C9FB56 (void);
// 0x000000C1 System.UInt64 Unity.Collections.xxHash3::Read64LE(System.Void*)
extern void xxHash3_Read64LE_m215CF04016BB327534223E32E2D4B451DDA1AACC (void);
// 0x000000C2 System.Void Unity.Collections.xxHash3::Write64LE(System.Void*,System.UInt64)
extern void xxHash3_Write64LE_mA44006BF77A500B0ADF6E3B2FC4CF238E7939E5D (void);
// 0x000000C3 System.UInt64 Unity.Collections.xxHash3::Mul32To64(System.UInt32,System.UInt32)
extern void xxHash3_Mul32To64_m8E59492BD1C7B9C66F5F907D165C27255EFC5A8F (void);
// 0x000000C4 System.UInt64 Unity.Collections.xxHash3::XorShift64(System.UInt64,System.Int32)
extern void xxHash3_XorShift64_m14CFB98B253542B96241F24A1BAA59BC660F10C2 (void);
// 0x000000C5 System.UInt64 Unity.Collections.xxHash3::Mul128Fold64(System.UInt64,System.UInt64)
extern void xxHash3_Mul128Fold64_mE4D677D2E0DACC2DAB3E33B876928A61E4E66036 (void);
// 0x000000C6 System.UInt64 Unity.Collections.xxHash3::Avalanche(System.UInt64)
extern void xxHash3_Avalanche_m8AB7E61594297A37DDBEB74555B9B6E937CD91B9 (void);
// 0x000000C7 System.UInt64 Unity.Collections.xxHash3::Mix2Acc(System.UInt64,System.UInt64,System.Byte*)
extern void xxHash3_Mix2Acc_mCC33F3919D7C753AC9997592EA6D6F95D35D72C3 (void);
// 0x000000C8 System.UInt64 Unity.Collections.xxHash3::MergeAcc(System.UInt64*,System.Byte*,System.UInt64)
extern void xxHash3_MergeAcc_mF5CA25B12DC5478CDA428FDDD8EDEA7B070B1CDD (void);
// 0x000000C9 System.Void Unity.Collections.xxHash3::DefaultHashLongInternalLoop(System.UInt64*,System.Byte*,System.Byte*,System.Int64,System.Byte*,System.Int32)
extern void xxHash3_DefaultHashLongInternalLoop_m25D46D5E62D5A75BF6B18D459552A4F639D7B070 (void);
// 0x000000CA System.Void Unity.Collections.xxHash3::DefaultAccumulate(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int64,System.Int32)
extern void xxHash3_DefaultAccumulate_m89B15352C5BBD7848BEF7D1DE2B93275637B22A9 (void);
// 0x000000CB System.Void Unity.Collections.xxHash3::DefaultAccumulate512(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int32)
extern void xxHash3_DefaultAccumulate512_mE2FA5E5ACA719367800C367B7B17CFAF94F38253 (void);
// 0x000000CC System.Void Unity.Collections.xxHash3::DefaultScrambleAcc(System.UInt64*,System.Byte*)
extern void xxHash3_DefaultScrambleAcc_m5F9168BDE58B95E6C98B107B341D0D822AAAEC37 (void);
// 0x000000CD System.UInt64 Unity.Collections.xxHash3::Hash64Long$BurstManaged(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void xxHash3_Hash64LongU24BurstManaged_m9385EE12B31C1382FF320BCDBE32657220250568 (void);
// 0x000000CE System.Void Unity.Collections.xxHash3::Hash128Long$BurstManaged(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void xxHash3_Hash128LongU24BurstManaged_mCC1E11085C5906FEAF9C4151C7A410701166563D (void);
// 0x000000CF System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Hash64Long_0000076AU24PostfixBurstDelegate__ctor_mB4E590E95DD92DB8F3B2E4B5056C79A5B4CB6039 (void);
// 0x000000D0 System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$PostfixBurstDelegate::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void Hash64Long_0000076AU24PostfixBurstDelegate_Invoke_mEBE891EF9C817CE04B09AF1B5834930530FA73BC (void);
// 0x000000D1 System.IAsyncResult Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$PostfixBurstDelegate::BeginInvoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,System.AsyncCallback,System.Object)
extern void Hash64Long_0000076AU24PostfixBurstDelegate_BeginInvoke_m2B0B2BCE0A34BD89A4408A8CA3DEC4F09D537E2B (void);
// 0x000000D2 System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Hash64Long_0000076AU24PostfixBurstDelegate_EndInvoke_m65C7236F1052B45E9013311C7572E43F85706F39 (void);
// 0x000000D3 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointerDiscard_m766542CA26A2A488488CE16DE600528552D22342 (void);
// 0x000000D4 System.IntPtr Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::GetFunctionPointer()
extern void Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointer_mD4DB2E000671100E8F6D832B1EE356FD90DE14DD (void);
// 0x000000D5 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::Constructor()
extern void Hash64Long_0000076AU24BurstDirectCall_Constructor_m38CB0CCB2F89D5DE9D8A768E7E6B8F6DFB97675F (void);
// 0x000000D6 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::Initialize()
extern void Hash64Long_0000076AU24BurstDirectCall_Initialize_m81FD1566ED90AF1394BD49CF55066DDB04D7A3B7 (void);
// 0x000000D7 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::.cctor()
extern void Hash64Long_0000076AU24BurstDirectCall__cctor_mF81FA641F594657093DC6649275573C06E93DAD3 (void);
// 0x000000D8 System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000076A$BurstDirectCall::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void Hash64Long_0000076AU24BurstDirectCall_Invoke_mBAF8FE023631CAEF10CDD01711D1E347CCD09DB0 (void);
// 0x000000D9 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Hash128Long_00000771U24PostfixBurstDelegate__ctor_m6177D6165894433A4DA7A0239097FE9EB2713F60 (void);
// 0x000000DA System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$PostfixBurstDelegate::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void Hash128Long_00000771U24PostfixBurstDelegate_Invoke_m98BD17513139C33AD4B344136AB31320E3F356F6 (void);
// 0x000000DB System.IAsyncResult Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$PostfixBurstDelegate::BeginInvoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&,System.AsyncCallback,System.Object)
extern void Hash128Long_00000771U24PostfixBurstDelegate_BeginInvoke_m440360353DBBF025F3E57B6637C7F03D3BE44150 (void);
// 0x000000DC System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Hash128Long_00000771U24PostfixBurstDelegate_EndInvoke_m817D40F455D1698D87DB51265D8418F21856FD9F (void);
// 0x000000DD System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Hash128Long_00000771U24BurstDirectCall_GetFunctionPointerDiscard_mD8EA149456EACFBE282AA22581E449DFFBBD1CDC (void);
// 0x000000DE System.IntPtr Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::GetFunctionPointer()
extern void Hash128Long_00000771U24BurstDirectCall_GetFunctionPointer_mD2081F94E13A5FCA1B248978B842077042C86AD9 (void);
// 0x000000DF System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::Constructor()
extern void Hash128Long_00000771U24BurstDirectCall_Constructor_m142B14A50874567BB385C60CA5FF6542BE07D229 (void);
// 0x000000E0 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::Initialize()
extern void Hash128Long_00000771U24BurstDirectCall_Initialize_mB0FD7FA9154EE619A30E546B13AFD52C332BEFBE (void);
// 0x000000E1 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::.cctor()
extern void Hash128Long_00000771U24BurstDirectCall__cctor_m31A781049CF6135C3BCFF4FDCB194F76E957E66A (void);
// 0x000000E2 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000771$BurstDirectCall::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void Hash128Long_00000771U24BurstDirectCall_Invoke_m55BBD8541377F59155C875479D4027A5E389A6C2 (void);
// 0x000000E3 System.Void $BurstDirectCallInitializer::Initialize()
extern void U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C (void);
static Il2CppMethodPointer s_methodPointers[227] = 
{
	EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D,
	IsUnmanagedAttribute__ctor_m05BDDBB49F005C47C815CD32668381083A1F5C43,
	AllocatorManager_CheckDelegate_m2034F06B2464A4A27E2B24F536B2F2244CFDC603,
	AllocatorManager_UseDelegate_m502251BCD61C58A4C55C3B51DC6DBC56B2F86981,
	AllocatorManager_allocate_block_mF37B0FD92BC1BF98C2A5C765E4F2AB9532300EE7,
	AllocatorManager_forward_mono_allocate_block_m490B419D75E5521086A86698341989DD47CBD8B6,
	AllocatorManager_LegacyOf_m9F8300F5AEED87F3B9FCD28C81A52B53D58B697B,
	AllocatorManager_TryLegacy_m7086EEBC033BDCAB24CAC49D695180EF1BA46895,
	AllocatorManager_Try_m67E1175F6B4340CF45D552DDC90CDFE0DA1912E3,
	AllocatorManager__cctor_m704686DC15590B3752449701AC79493E5DBB4681,
	TryFunction__ctor_m61B0A7506F9D0C752E09C91E3E3CC16034A41AA0,
	TryFunction_Invoke_m62DB13101BCEC040485DBD4F68E9B4B9406368DE,
	TryFunction_BeginInvoke_mDB5760D98471188127E578B6598BBD7D498ACCF2,
	TryFunction_EndInvoke_m26A14616127F9673336FE6BB8C1202F6A7F27C44,
	AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D,
	AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060,
	AllocatorHandle_op_Implicit_m63D8E96033A00071E8FDEC80D6956ADBE627067C,
	AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B,
	AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E,
	AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF,
	AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1,
	Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42,
	Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA,
	Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907,
	Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF,
	Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730,
	Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9,
	Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1,
	StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D,
	StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B,
	StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678,
	StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B,
	Try_00000A0DU24PostfixBurstDelegate__ctor_m6CA496ED55E8C0A79AF1416EACA73B7DC105A1E8,
	Try_00000A0DU24PostfixBurstDelegate_Invoke_m14771936EA82A88235522B9505C4ED11040C3E12,
	Try_00000A0DU24PostfixBurstDelegate_BeginInvoke_mB5F366704B329E5C7ACC3AA5EC77C546DE0975DB,
	Try_00000A0DU24PostfixBurstDelegate_EndInvoke_mDFD46823A5E6964B98E6941C9E19CE53D792EA91,
	Try_00000A0DU24BurstDirectCall_GetFunctionPointerDiscard_m84BAF84015A781A693374369955208145DCF1287,
	Try_00000A0DU24BurstDirectCall_GetFunctionPointer_m5105FA31F2C62F8E25F93557CCBCDBDB8097B304,
	Try_00000A0DU24BurstDirectCall_Constructor_mC9E73E28C439C59971AB2CCFD142B3C545DA351D,
	Try_00000A0DU24BurstDirectCall_Initialize_mAA683C51A2EBF891DB5B0979D334D683FDB2AA7B,
	Try_00000A0DU24BurstDirectCall__cctor_m23A5E5C3C2CB347DB2BF1C9F1BA78FCEF0A667D3,
	Try_00000A0DU24BurstDirectCall_Invoke_m5E38DA0377AC51E89FC0B77A91DA26BB95820C48,
	SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685,
	SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3,
	SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8,
	SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F,
	SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5,
	Try_00000A1BU24PostfixBurstDelegate__ctor_mD55E80D851C62D0199833375E332FB8568804DE3,
	Try_00000A1BU24PostfixBurstDelegate_Invoke_mC5B849FCF13EC1E9902C5F88F7C0E467F1676E45,
	Try_00000A1BU24PostfixBurstDelegate_BeginInvoke_m417507BF7BA1FF01C7E671A652BCE33B2785E1F5,
	Try_00000A1BU24PostfixBurstDelegate_EndInvoke_mA5EFF693AF5DA68E98F99AB057ADC0681BBF3544,
	Try_00000A1BU24BurstDirectCall_GetFunctionPointerDiscard_mA13A97454F15CEA9E01D5DAE9885D64405BDC160,
	Try_00000A1BU24BurstDirectCall_GetFunctionPointer_m55DFEEEAD2C4157915074EB6B0CFA639BD4DE29F,
	Try_00000A1BU24BurstDirectCall_Constructor_m082EAA29ABF433DE3AE1809D8FEF35798D9AC630,
	Try_00000A1BU24BurstDirectCall_Initialize_m6CA9079BFC71C764220C98C5F1B2D4E3117F5891,
	Try_00000A1BU24BurstDirectCall__cctor_m437D4396F3B6ABD61A66D354825A25B1908576AE,
	Try_00000A1BU24BurstDirectCall_Invoke_m365F55D30BBADEEB5CBC42E9E010BF66363886F0,
	NULL,
	TableEntry__cctor_m4E930B1CD3F5349C2EE468379393D62B7F40908F,
	Managed__cctor_mDB5004E87DDAA2A1039890601B936875DECCD4EE,
	CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68,
	BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A,
	BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C,
	NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63,
	CollectionHelper_Hash_m8132774EA770BB87BF148F3B4900F64CCA47F576,
	CollectionHelper_AssumePositive_mD8785D4C9E69993993EA9982A5F6F2ADAFE5B110,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Unmanaged_Allocate_mE42F0479C571BE76773614A408329CFCB51FB7F5,
	Unmanaged_Free_mE141FBBD8FC68C23121409BD2187EDDAB7849045,
	NULL,
	Array_IsCustom_m5553247492EBFB5ECECF39E326F1EC5C3F7FA892,
	Array_CustomResize_mD9B4173A8E9495BECF4359DE1B09E72428F2C521,
	Array_Resize_m7334DEAC65445477E1D278AC2DFEBD53C06C2C1B,
	NULL,
	Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1,
	Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32,
	NULL,
	NULL,
	RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580,
	RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678,
	RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE,
	RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066,
	RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE,
	MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3,
	MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958,
	MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF,
	MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F,
	MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB,
	Try_00000733U24PostfixBurstDelegate__ctor_mC269FA37E2C581177FDAB83EBAD30A32DAC72195,
	Try_00000733U24PostfixBurstDelegate_Invoke_m557F3B6E7AA123BE278B8A696F52F1C131428479,
	Try_00000733U24PostfixBurstDelegate_BeginInvoke_m90138D17DDDBF0F9E33B6F487300B6F4809965AE,
	Try_00000733U24PostfixBurstDelegate_EndInvoke_m66C4B08A34BC81B9C7672D23353625FFF0B6C537,
	Try_00000733U24BurstDirectCall_GetFunctionPointerDiscard_m890205B4036748F9AB5D9A0C4FA7CE2C44DEF745,
	Try_00000733U24BurstDirectCall_GetFunctionPointer_m06ECE770F48FFF3B3D94FCA18C32F79DCB28B459,
	Try_00000733U24BurstDirectCall_Constructor_mA2CE9EEA9180483974A6EC4D6A9D36DB03734680,
	Try_00000733U24BurstDirectCall_Initialize_m8F25AF68D2CB64DA669895C921739A9CF99A9ECE,
	Try_00000733U24BurstDirectCall__cctor_m3511B05AC0CB0C1AB71F4E4224B3E082991DE4DD,
	Try_00000733U24BurstDirectCall_Invoke_m4FEDD13AEB6796E178E5255555EB79DF67E91FE2,
	xxHash3_Avx2HashLongInternalLoop_mCCB0DF801328F7B4907CCD550BA0E9C2A971E4AF,
	xxHash3_Avx2ScrambleAcc_mA6DBBF7E016635A901F80FC07FF1851618D6C3D5,
	xxHash3_Avx2Accumulate_m140D43D5123949B4A98F707CE310C744F78BC35B,
	xxHash3_Avx2Accumulate512_m7C23025DBFD191FE18600B12063730E6739850C7,
	xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7,
	xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D,
	xxHash3_ToUint4_m8417C90298E7430A8DCD51069984F8AE71C9FB56,
	xxHash3_Read64LE_m215CF04016BB327534223E32E2D4B451DDA1AACC,
	xxHash3_Write64LE_mA44006BF77A500B0ADF6E3B2FC4CF238E7939E5D,
	xxHash3_Mul32To64_m8E59492BD1C7B9C66F5F907D165C27255EFC5A8F,
	xxHash3_XorShift64_m14CFB98B253542B96241F24A1BAA59BC660F10C2,
	xxHash3_Mul128Fold64_mE4D677D2E0DACC2DAB3E33B876928A61E4E66036,
	xxHash3_Avalanche_m8AB7E61594297A37DDBEB74555B9B6E937CD91B9,
	xxHash3_Mix2Acc_mCC33F3919D7C753AC9997592EA6D6F95D35D72C3,
	xxHash3_MergeAcc_mF5CA25B12DC5478CDA428FDDD8EDEA7B070B1CDD,
	xxHash3_DefaultHashLongInternalLoop_m25D46D5E62D5A75BF6B18D459552A4F639D7B070,
	xxHash3_DefaultAccumulate_m89B15352C5BBD7848BEF7D1DE2B93275637B22A9,
	xxHash3_DefaultAccumulate512_mE2FA5E5ACA719367800C367B7B17CFAF94F38253,
	xxHash3_DefaultScrambleAcc_m5F9168BDE58B95E6C98B107B341D0D822AAAEC37,
	xxHash3_Hash64LongU24BurstManaged_m9385EE12B31C1382FF320BCDBE32657220250568,
	xxHash3_Hash128LongU24BurstManaged_mCC1E11085C5906FEAF9C4151C7A410701166563D,
	Hash64Long_0000076AU24PostfixBurstDelegate__ctor_mB4E590E95DD92DB8F3B2E4B5056C79A5B4CB6039,
	Hash64Long_0000076AU24PostfixBurstDelegate_Invoke_mEBE891EF9C817CE04B09AF1B5834930530FA73BC,
	Hash64Long_0000076AU24PostfixBurstDelegate_BeginInvoke_m2B0B2BCE0A34BD89A4408A8CA3DEC4F09D537E2B,
	Hash64Long_0000076AU24PostfixBurstDelegate_EndInvoke_m65C7236F1052B45E9013311C7572E43F85706F39,
	Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointerDiscard_m766542CA26A2A488488CE16DE600528552D22342,
	Hash64Long_0000076AU24BurstDirectCall_GetFunctionPointer_mD4DB2E000671100E8F6D832B1EE356FD90DE14DD,
	Hash64Long_0000076AU24BurstDirectCall_Constructor_m38CB0CCB2F89D5DE9D8A768E7E6B8F6DFB97675F,
	Hash64Long_0000076AU24BurstDirectCall_Initialize_m81FD1566ED90AF1394BD49CF55066DDB04D7A3B7,
	Hash64Long_0000076AU24BurstDirectCall__cctor_mF81FA641F594657093DC6649275573C06E93DAD3,
	Hash64Long_0000076AU24BurstDirectCall_Invoke_mBAF8FE023631CAEF10CDD01711D1E347CCD09DB0,
	Hash128Long_00000771U24PostfixBurstDelegate__ctor_m6177D6165894433A4DA7A0239097FE9EB2713F60,
	Hash128Long_00000771U24PostfixBurstDelegate_Invoke_m98BD17513139C33AD4B344136AB31320E3F356F6,
	Hash128Long_00000771U24PostfixBurstDelegate_BeginInvoke_m440360353DBBF025F3E57B6637C7F03D3BE44150,
	Hash128Long_00000771U24PostfixBurstDelegate_EndInvoke_m817D40F455D1698D87DB51265D8418F21856FD9F,
	Hash128Long_00000771U24BurstDirectCall_GetFunctionPointerDiscard_mD8EA149456EACFBE282AA22581E449DFFBBD1CDC,
	Hash128Long_00000771U24BurstDirectCall_GetFunctionPointer_mD2081F94E13A5FCA1B248978B842077042C86AD9,
	Hash128Long_00000771U24BurstDirectCall_Constructor_m142B14A50874567BB385C60CA5FF6542BE07D229,
	Hash128Long_00000771U24BurstDirectCall_Initialize_mB0FD7FA9154EE619A30E546B13AFD52C332BEFBE,
	Hash128Long_00000771U24BurstDirectCall__cctor_m31A781049CF6135C3BCFF4FDCB194F76E957E66A,
	Hash128Long_00000771U24BurstDirectCall_Invoke_m55BBD8541377F59155C875479D4027A5E389A6C2,
	U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C,
};
extern void AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D_AdjustorThunk (void);
extern void AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060_AdjustorThunk (void);
extern void AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B_AdjustorThunk (void);
extern void AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E_AdjustorThunk (void);
extern void AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF_AdjustorThunk (void);
extern void AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1_AdjustorThunk (void);
extern void Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42_AdjustorThunk (void);
extern void Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA_AdjustorThunk (void);
extern void Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907_AdjustorThunk (void);
extern void Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF_AdjustorThunk (void);
extern void Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730_AdjustorThunk (void);
extern void Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9_AdjustorThunk (void);
extern void Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1_AdjustorThunk (void);
extern void StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D_AdjustorThunk (void);
extern void StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678_AdjustorThunk (void);
extern void SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685_AdjustorThunk (void);
extern void SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3_AdjustorThunk (void);
extern void SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F_AdjustorThunk (void);
extern void Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1_AdjustorThunk (void);
extern void Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32_AdjustorThunk (void);
extern void RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580_AdjustorThunk (void);
extern void RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678_AdjustorThunk (void);
extern void RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE_AdjustorThunk (void);
extern void MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3_AdjustorThunk (void);
extern void MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958_AdjustorThunk (void);
extern void MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF_AdjustorThunk (void);
extern void MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F_AdjustorThunk (void);
extern void MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[28] = 
{
	{ 0x0600000F, AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D_AdjustorThunk },
	{ 0x06000010, AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060_AdjustorThunk },
	{ 0x06000012, AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B_AdjustorThunk },
	{ 0x06000013, AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E_AdjustorThunk },
	{ 0x06000014, AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF_AdjustorThunk },
	{ 0x06000015, AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1_AdjustorThunk },
	{ 0x06000016, Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42_AdjustorThunk },
	{ 0x06000017, Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA_AdjustorThunk },
	{ 0x06000018, Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907_AdjustorThunk },
	{ 0x06000019, Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF_AdjustorThunk },
	{ 0x0600001A, Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730_AdjustorThunk },
	{ 0x0600001B, Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9_AdjustorThunk },
	{ 0x0600001C, Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1_AdjustorThunk },
	{ 0x0600001D, StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D_AdjustorThunk },
	{ 0x0600001F, StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678_AdjustorThunk },
	{ 0x0600002B, SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685_AdjustorThunk },
	{ 0x0600002C, SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3_AdjustorThunk },
	{ 0x0600002E, SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F_AdjustorThunk },
	{ 0x060000A2, Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1_AdjustorThunk },
	{ 0x060000A3, Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32_AdjustorThunk },
	{ 0x060000A6, RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580_AdjustorThunk },
	{ 0x060000A7, RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678_AdjustorThunk },
	{ 0x060000A8, RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE_AdjustorThunk },
	{ 0x060000AB, MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3_AdjustorThunk },
	{ 0x060000AC, MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958_AdjustorThunk },
	{ 0x060000AD, MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF_AdjustorThunk },
	{ 0x060000AE, MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F_AdjustorThunk },
	{ 0x060000AF, MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB_AdjustorThunk },
};
static const int32_t s_InvokerIndices[227] = 
{
	2586,
	2586,
	4067,
	4129,
	3918,
	3745,
	3931,
	3918,
	3918,
	4140,
	1194,
	791,
	332,
	769,
	2483,
	2586,
	4096,
	2524,
	2610,
	2524,
	2586,
	2586,
	2525,
	2525,
	2524,
	2120,
	2586,
	2524,
	1421,
	3480,
	2586,
	3480,
	1194,
	791,
	332,
	1463,
	4067,
	4116,
	4140,
	4140,
	4140,
	3480,
	2524,
	1421,
	3480,
	2586,
	3480,
	1194,
	791,
	332,
	1463,
	4067,
	4116,
	4140,
	4140,
	4140,
	3480,
	-1,
	4140,
	4140,
	2586,
	2135,
	2586,
	2586,
	3471,
	3922,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3145,
	3751,
	-1,
	4032,
	2692,
	2692,
	-1,
	2586,
	2586,
	-1,
	-1,
	2586,
	2586,
	1421,
	3480,
	3480,
	2121,
	2586,
	2586,
	1421,
	1796,
	1194,
	791,
	332,
	1463,
	4067,
	4116,
	4140,
	4140,
	4140,
	3480,
	2736,
	3745,
	2735,
	3080,
	2973,
	2890,
	3805,
	3932,
	3747,
	3495,
	3496,
	3497,
	3937,
	3186,
	3182,
	2736,
	2735,
	2889,
	3745,
	2973,
	2890,
	1194,
	311,
	88,
	1533,
	4067,
	4116,
	4140,
	4140,
	4140,
	2973,
	1194,
	174,
	60,
	2135,
	4067,
	4116,
	4140,
	4140,
	4140,
	2890,
	4140,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[6] = 
{
	{ 0x0600001E, 13,  (void**)&StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B_RuntimeMethod_var, 0 },
	{ 0x06000020, 14,  (void**)&StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B_RuntimeMethod_var, 0 },
	{ 0x0600002D, 11,  (void**)&SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8_RuntimeMethod_var, 0 },
	{ 0x0600002F, 12,  (void**)&SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5_RuntimeMethod_var, 0 },
	{ 0x060000A9, 2,  (void**)&RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066_RuntimeMethod_var, 0 },
	{ 0x060000AA, 3,  (void**)&RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000013, { 0, 2 } },
	{ 0x0200001C, { 3, 25 } },
	{ 0x0200001E, { 28, 25 } },
	{ 0x02000020, { 53, 25 } },
	{ 0x02000022, { 78, 25 } },
	{ 0x02000024, { 103, 27 } },
	{ 0x02000030, { 133, 2 } },
	{ 0x06000043, { 2, 1 } },
	{ 0x0600009D, { 130, 1 } },
	{ 0x060000A1, { 131, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[135] = 
{
	{ (Il2CppRGCTXDataType)2, 417 },
	{ (Il2CppRGCTXDataType)3, 15639 },
	{ (Il2CppRGCTXDataType)3, 15685 },
	{ (Il2CppRGCTXDataType)3, 5247 },
	{ (Il2CppRGCTXDataType)3, 15697 },
	{ (Il2CppRGCTXDataType)3, 15026 },
	{ (Il2CppRGCTXDataType)3, 5246 },
	{ (Il2CppRGCTXDataType)3, 5248 },
	{ (Il2CppRGCTXDataType)2, 313 },
	{ (Il2CppRGCTXDataType)3, 5237 },
	{ (Il2CppRGCTXDataType)3, 5340 },
	{ (Il2CppRGCTXDataType)3, 5240 },
	{ (Il2CppRGCTXDataType)3, 5214 },
	{ (Il2CppRGCTXDataType)3, 5236 },
	{ (Il2CppRGCTXDataType)3, 5306 },
	{ (Il2CppRGCTXDataType)3, 5239 },
	{ (Il2CppRGCTXDataType)3, 5270 },
	{ (Il2CppRGCTXDataType)3, 5238 },
	{ (Il2CppRGCTXDataType)2, 1126 },
	{ (Il2CppRGCTXDataType)3, 5242 },
	{ (Il2CppRGCTXDataType)2, 1142 },
	{ (Il2CppRGCTXDataType)3, 5245 },
	{ (Il2CppRGCTXDataType)2, 1121 },
	{ (Il2CppRGCTXDataType)3, 5241 },
	{ (Il2CppRGCTXDataType)2, 1137 },
	{ (Il2CppRGCTXDataType)3, 5244 },
	{ (Il2CppRGCTXDataType)2, 1131 },
	{ (Il2CppRGCTXDataType)3, 5243 },
	{ (Il2CppRGCTXDataType)3, 5354 },
	{ (Il2CppRGCTXDataType)3, 15700 },
	{ (Il2CppRGCTXDataType)3, 15029 },
	{ (Il2CppRGCTXDataType)3, 5353 },
	{ (Il2CppRGCTXDataType)3, 5355 },
	{ (Il2CppRGCTXDataType)3, 5251 },
	{ (Il2CppRGCTXDataType)2, 316 },
	{ (Il2CppRGCTXDataType)3, 5344 },
	{ (Il2CppRGCTXDataType)3, 5347 },
	{ (Il2CppRGCTXDataType)3, 5217 },
	{ (Il2CppRGCTXDataType)3, 5343 },
	{ (Il2CppRGCTXDataType)3, 5321 },
	{ (Il2CppRGCTXDataType)3, 5346 },
	{ (Il2CppRGCTXDataType)3, 5285 },
	{ (Il2CppRGCTXDataType)3, 5345 },
	{ (Il2CppRGCTXDataType)2, 1129 },
	{ (Il2CppRGCTXDataType)3, 5349 },
	{ (Il2CppRGCTXDataType)2, 1145 },
	{ (Il2CppRGCTXDataType)3, 5352 },
	{ (Il2CppRGCTXDataType)2, 1124 },
	{ (Il2CppRGCTXDataType)3, 5348 },
	{ (Il2CppRGCTXDataType)2, 1140 },
	{ (Il2CppRGCTXDataType)3, 5351 },
	{ (Il2CppRGCTXDataType)2, 1134 },
	{ (Il2CppRGCTXDataType)3, 5350 },
	{ (Il2CppRGCTXDataType)3, 5212 },
	{ (Il2CppRGCTXDataType)3, 15696 },
	{ (Il2CppRGCTXDataType)3, 15025 },
	{ (Il2CppRGCTXDataType)3, 5211 },
	{ (Il2CppRGCTXDataType)3, 5213 },
	{ (Il2CppRGCTXDataType)3, 5235 },
	{ (Il2CppRGCTXDataType)2, 312 },
	{ (Il2CppRGCTXDataType)3, 5202 },
	{ (Il2CppRGCTXDataType)3, 5339 },
	{ (Il2CppRGCTXDataType)3, 5205 },
	{ (Il2CppRGCTXDataType)3, 5201 },
	{ (Il2CppRGCTXDataType)3, 5305 },
	{ (Il2CppRGCTXDataType)3, 5204 },
	{ (Il2CppRGCTXDataType)3, 5269 },
	{ (Il2CppRGCTXDataType)3, 5203 },
	{ (Il2CppRGCTXDataType)2, 1125 },
	{ (Il2CppRGCTXDataType)3, 5207 },
	{ (Il2CppRGCTXDataType)2, 1141 },
	{ (Il2CppRGCTXDataType)3, 5210 },
	{ (Il2CppRGCTXDataType)2, 1120 },
	{ (Il2CppRGCTXDataType)3, 5206 },
	{ (Il2CppRGCTXDataType)2, 1136 },
	{ (Il2CppRGCTXDataType)3, 5209 },
	{ (Il2CppRGCTXDataType)2, 1130 },
	{ (Il2CppRGCTXDataType)3, 5208 },
	{ (Il2CppRGCTXDataType)3, 5319 },
	{ (Il2CppRGCTXDataType)3, 15699 },
	{ (Il2CppRGCTXDataType)3, 15028 },
	{ (Il2CppRGCTXDataType)3, 5318 },
	{ (Il2CppRGCTXDataType)3, 5320 },
	{ (Il2CppRGCTXDataType)3, 5250 },
	{ (Il2CppRGCTXDataType)2, 315 },
	{ (Il2CppRGCTXDataType)3, 5309 },
	{ (Il2CppRGCTXDataType)3, 5342 },
	{ (Il2CppRGCTXDataType)3, 5312 },
	{ (Il2CppRGCTXDataType)3, 5216 },
	{ (Il2CppRGCTXDataType)3, 5308 },
	{ (Il2CppRGCTXDataType)3, 5311 },
	{ (Il2CppRGCTXDataType)3, 5284 },
	{ (Il2CppRGCTXDataType)3, 5310 },
	{ (Il2CppRGCTXDataType)2, 1128 },
	{ (Il2CppRGCTXDataType)3, 5314 },
	{ (Il2CppRGCTXDataType)2, 1144 },
	{ (Il2CppRGCTXDataType)3, 5317 },
	{ (Il2CppRGCTXDataType)2, 1123 },
	{ (Il2CppRGCTXDataType)3, 5313 },
	{ (Il2CppRGCTXDataType)2, 1139 },
	{ (Il2CppRGCTXDataType)3, 5316 },
	{ (Il2CppRGCTXDataType)2, 1133 },
	{ (Il2CppRGCTXDataType)3, 5315 },
	{ (Il2CppRGCTXDataType)3, 5282 },
	{ (Il2CppRGCTXDataType)3, 15698 },
	{ (Il2CppRGCTXDataType)3, 15027 },
	{ (Il2CppRGCTXDataType)3, 5281 },
	{ (Il2CppRGCTXDataType)3, 15641 },
	{ (Il2CppRGCTXDataType)3, 15776 },
	{ (Il2CppRGCTXDataType)3, 5283 },
	{ (Il2CppRGCTXDataType)3, 5249 },
	{ (Il2CppRGCTXDataType)2, 314 },
	{ (Il2CppRGCTXDataType)3, 5272 },
	{ (Il2CppRGCTXDataType)3, 5341 },
	{ (Il2CppRGCTXDataType)3, 5275 },
	{ (Il2CppRGCTXDataType)3, 5215 },
	{ (Il2CppRGCTXDataType)3, 5271 },
	{ (Il2CppRGCTXDataType)3, 5307 },
	{ (Il2CppRGCTXDataType)3, 5274 },
	{ (Il2CppRGCTXDataType)3, 5273 },
	{ (Il2CppRGCTXDataType)2, 1127 },
	{ (Il2CppRGCTXDataType)3, 5277 },
	{ (Il2CppRGCTXDataType)2, 1143 },
	{ (Il2CppRGCTXDataType)3, 5280 },
	{ (Il2CppRGCTXDataType)2, 1122 },
	{ (Il2CppRGCTXDataType)3, 5276 },
	{ (Il2CppRGCTXDataType)2, 1138 },
	{ (Il2CppRGCTXDataType)3, 5279 },
	{ (Il2CppRGCTXDataType)2, 1132 },
	{ (Il2CppRGCTXDataType)3, 5278 },
	{ (Il2CppRGCTXDataType)3, 15922 },
	{ (Il2CppRGCTXDataType)3, 15695 },
	{ (Il2CppRGCTXDataType)3, 15602 },
	{ (Il2CppRGCTXDataType)3, 15918 },
	{ (Il2CppRGCTXDataType)2, 412 },
};
extern const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_Collections_CodeGenModule;
const Il2CppCodeGenModule g_Unity_Collections_CodeGenModule = 
{
	"Unity.Collections.dll",
	227,
	s_methodPointers,
	28,
	s_adjustorThunks,
	s_InvokerIndices,
	6,
	s_reversePInvokeIndices,
	10,
	s_rgctxIndices,
	135,
	s_rgctxValues,
	NULL,
	g_Unity_Collections_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
