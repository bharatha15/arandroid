using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject 
{
    #region Private Fields
    private static Dictionary<string, GameObject> spawnObjects = new Dictionary<string, GameObject>();
    private static List<GameObject> spawnObjectsList = new List<GameObject>();
    #endregion

    #region Public Properties
    public static Dictionary<string, GameObject> SpawnObjects
    {
        set { spawnObjects = value; }
        get { return spawnObjects; }
    }

    public static List<GameObject> SpawnObjectList
    {
        set { spawnObjectsList = value; }
        get { return spawnObjectsList; }
    }
    #endregion

}
