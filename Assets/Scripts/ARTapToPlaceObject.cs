using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using System;
using UnityEngine.XR.ARSubsystems;

public class ARTapToPlaceObject : MonoBehaviour
{
    #region Serialized Private Fields
    [Header("Script")]
    [SerializeField] private EventHandler eventHandler;
    [SerializeField] private RaycastSelection raycastSelection;
    [SerializeField] private ToolTip toolTip;
    #endregion

    #region Public Fields
    public GameObject objectToPlace;
    public GameObject placementIndicator;
    #endregion

    #region Private Fields
    // private ARSessionOrigin arOrigin;
    private ARRaycastManager arOrigin;

    private Pose placementPose;
    private bool placementPoseIsValid = false;

    bool isPlaced = false;
    bool isInstantiated = false;

    Vector3 instantiatedPosition;
    Quaternion instantiatedRotation;
    Vector3 instantiatedScale;


    List<ARRaycastHit> hits = new List<ARRaycastHit>();
    GameObject spawnedObject;
    Camera arCamera;
    #endregion

    #region Public Properties
    public void SetIsObjectPlaced(bool value)
    {
        isPlaced = value;
    }

    public bool GetIsObjectPlaced()
    {
        return isPlaced;
    }

    public bool Instantiated
    {
        set { isInstantiated = value; }
        get { return isInstantiated; }
    }

    public Vector3 InstantiatedPosition
    {
        set { instantiatedPosition = value; }
        get { return instantiatedPosition; }
    }

    public Quaternion InstantiatedRotation
    {
        set { instantiatedRotation = value; }
        get { return instantiatedRotation; }
    }

    public Vector3 InstantiatedScale
    {
        set { instantiatedScale = value; }
        get { return instantiatedScale; }
    }



    /// <summary>
    /// Returns the Tap on placed Game Object
    /// </summary>
    /// <returns></returns>
    public GameObject GetSpawnedObject()
    {
        return spawnedObject;
    }

    /// <summary>
    /// Placement Indicator Gameobject.
    /// </summary>
    public GameObject PlacementIndicator
    {
        get { return placementIndicator; }
    }

    #endregion



    #region Monobehaviour Callbacks
    private void Awake()
    {
        eventHandler.ToggleResetPrefabBtn(false);
    }

    void Start()
    {
        //arOrigin = FindObjectOfType<ARSessionOrigin>();
        arOrigin = FindObjectOfType<ARRaycastManager>();

        PlacementIndicator.transform.tag = "Placement Indicator";
        arCamera = Camera.main.gameObject.GetComponent<Camera>();

        Application.targetFrameRate = 100;

        PlacementIndicator.SetActive(false);

        SetIsObjectPlaced(false);
    }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();


        /* Tap and Place the Object only once. */
        if (placementPoseIsValid && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && GetIsObjectPlaced().Equals(false))
        {
            PlaceObject();
        }

    }
    #endregion 


    /// <summary>
    /// Tap and Place the Object on the Placement Indicator.
    /// </summary>
    private void PlaceObject()
    {
        // spawnedObject = objectToPlace;
        Ray ray = arCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag(PlacementIndicator.transform.tag))
            {
                switch (Instantiated)
                {
                    case false:
                        InstantiatedPosition = placementPose.position;
                        InstantiatedRotation = placementPose.rotation;

                        spawnedObject = Instantiate(objectToPlace, placementPose.position, placementPose.rotation);

                        InstantiatedScale = spawnedObject.transform.localScale;


                        eventHandler.ToggleResetPrefabBtn(true);

                        spawnedObject.tag = "cube";

                        SpawnObject.SpawnObjects.Add(spawnedObject.tag, spawnedObject);
                        SpawnObject.SpawnObjectList.Add(spawnedObject);

                        StartCoroutine(RaycastSelectionToggle(true));

                        // placementIndicator.SetActive(false);

                        SetIsObjectPlaced(true);

                        Instantiated = true;
                        break;

                    case true:
                        ToggleSpawnedObject(true, hit);
                        SetIsObjectPlaced(true);
                        StartCoroutine(RaycastSelectionToggle(true));
                        break;

                }

            }
        }



    }

    /// <summary>
    /// Toggle Raycast Selection Script
    /// </summary>
    /// <param name="value">Enable or disable the RayCastSelection script</param>
    /// <returns></returns>
    IEnumerator RaycastSelectionToggle(bool value)
    {
        yield return new WaitForSeconds(0.1f);
        raycastSelection.enabled = value;
        raycastSelection.TargetImageToggle(value);
        raycastSelection.FingerPointerSelector.SetActive(value);
        PlacementIndicator.SetActive(false);

    }

    /// <summary>
    /// Once the UpdatePlacementPose() method recognises the surface, then enable the 
    /// Placement Indicator PNG image 
    /// </summary>
    private void UpdatePlacementIndicator()
    {

        if (placementPoseIsValid && GetIsObjectPlaced() == false) // && 
        {
            PlacementIndicator.SetActive(true);
            PlacementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else if(placementPoseIsValid && GetIsObjectPlaced() == true && Instantiated == true) //
        {
            PlacementIndicator.SetActive(false);
        }

    }


    /// <summary>
    /// Check whether the Rays coming from the Camera View port is hitting the 
    /// surface, if YES then Hit Rotation will be calculated
    /// </summary>
    private void UpdatePlacementPose()
    {
        var screenCenter = arCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        arOrigin.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }




    /// <summary>
    /// Event Listener for Reset Button
    /// Rest for tap and place the object, once again 
    /// </summary>
    public void RePlaceObject()
    {
        Debug.Log("Reset of Prefab event is working.....");

        foreach (var obj in SpawnObject.SpawnObjectList)
        {
            obj.SetActive(false);
        }

        // Instantiated = true;
        SetIsObjectPlaced(false);
        toolTip.ToolTipTextEnabled = false;
        toolTip.ToolTipText.SetActive(false);
        raycastSelection.TargetImageToggle(false);

        raycastSelection.FingerPointerSelector.SetActive(false);
    }

    /// <summary>
    /// Activate or Deactivate Spawned Object
    /// </summary>
    public void ToggleSpawnedObject(bool value, RaycastHit hit)
    {
        foreach (var obj in SpawnObject.SpawnObjectList)
        {
            obj.SetActive(value);
            obj.transform.position = hit.transform.position;
            obj.transform.rotation = hit.transform.rotation;
        }
 
    }


    private void OnDestroy()
    {
        
    }
}
