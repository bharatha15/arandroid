using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolTip : MonoBehaviour
{

    #region Serialized Private Fields
    [Header("Script")]
    [SerializeField] private GameObject toolTipButton;
    [SerializeField] private RaycastSelection raycastSelection;
    [SerializeField] private ARTapToPlaceObject aRTapToPlaceObject;

    [SerializeField] private GameObject toolTipText;
    #endregion

    #region Public Properties
    public GameObject ToolTipText
    {
        get { return toolTipText; }
    }

    /// <summary>
    /// Enable or Disable the ToolTip Text
    /// </summary>
    public bool ToolTipTextEnabled
    {
        set; get;
    }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        // raycastSelection = new RaycastSelection();

        ToolTipButtonToggle(false);
    }

    // Update is called once per frame
    void Update()
    {
        ToolTipLookAtCamera();
    }

    private void FixedUpdate()
    {
        
    }

    #region Public Methods

    /// <summary>
    /// Enable or Disable Tooltip Image
    /// </summary>
    /// <param name="value"></param>
    public void ToolTipButtonToggle(bool value)
    {
        toolTipButton.SetActive(value);
        ToolTipLookAtCamera();
    }

    /// <summary>
    /// Button Event Information of the Gameobject via ToolTip Button Press
    /// </summary>
    public void ToolTipInfo()
    {
        ObjectInfo();
    }

    /// <summary>
    /// Gameobject Information.
    /// </summary>
    public void ObjectInfo()
    {
        Ray ray = Camera.main.ScreenPointToRay(raycastSelection.TargetImage.transform.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) //ray
        {
            // hit.collider.gameObject.SetActive(false);
            toolTipText.SetActive(true);
            float x = hit.collider.transform.position.x;
            float y = hit.collider.transform.position.y + hit.collider.transform.localScale.y + 0.1f;
            float z = hit.collider.transform.position.z;
            toolTipText.transform.position = new Vector3(x, y, z);


            var lineRenderer = toolTipText.GetComponent<LineRenderer>();
            lineRenderer.SetPosition(0, hit.collider.transform.position);
            lineRenderer.SetPosition(1, toolTipText.transform.position);
            
            lineRenderer.material.color = Color.black;
            lineRenderer.startWidth = 0.001f;
            lineRenderer.endWidth = 0.001f;
            
            ToolTipLookAtCamera();

            ToolTipTextEnabled = true;
            // toolTipText.transform.SetParent(aRTapToPlaceObject.GetSpawnedObject().transform);
        }
    }

    public void ToolTipLookAtCamera()
    {
        toolTipText.transform.LookAt(Camera.main.transform.forward);
    }


    #endregion

}
