using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    #region

    /// <summary>
    /// Scripts
    /// </summary>
    [Header("Script")]
    [SerializeField] private ARTapToPlaceObject aRTapToPlaceObject;
    [SerializeField] private AnchorHandler anchorHandler;

    [Header("Game Objects")]
    [SerializeField] private GameObject rotateObjButton;
    [SerializeField] private GameObject anchorButton;

    
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rotateObjButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (aRTapToPlaceObject.GetSpawnedObject())
        {
            rotateObjButton.SetActive(true);
        }
        
    }
}
