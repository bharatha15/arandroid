using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : MonoBehaviour
{

    [SerializeField] private RaycastSelection raycastSelection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(raycastSelection.ObjectSelected == true)
        {
            GameObject obj = raycastSelection.SelectObject();
            GameObject target = raycastSelection.TargetImage;

            float X_Offset = Mathf.Round(target.transform.position.x - obj.transform.position.x);
            float Y_Offset = Mathf.Round(target.transform.position.y - obj.transform.position.y);
            float Z_Offset = Mathf.Round(target.transform.position.z - obj.transform.position.z);


            obj.SetActive(false);
        }
    }
}
