using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastSelection : MonoBehaviour
{

    #region Private Serialized Fields
    [Header("Script")]
    [SerializeField] private ARTapToPlaceObject aRTapToPlaceObject;
    [SerializeField] private ToolTip toolTip;
         
    
    /// <summary>
    /// Target image UI
    /// </summary>
    [SerializeField] private GameObject targetImage;
    [SerializeField] private GameObject fingerPointerSelector;
    #endregion

    #region Private Fields
    SpawnObject spawnObject = new SpawnObject();
    #endregion

    #region Public Properties
    public GameObject TargetImage
    {
        get { return targetImage; }
    }

    public GameObject FingerPointerSelector
    {
        get { return fingerPointerSelector; }
    }


    public bool ObjectSelected
    {
        set; get;
    }
    #endregion

    #region Monobehaviour Callbacks

    // Start is called before the first frame update
    void Start()
    {
        TargetImageToggle(false);
        FingerPointerSelector.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(targetImage.transform.position);   
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit)) //ray
        {
            if(hit.collider.tag == "cube" && toolTip.ToolTipTextEnabled == true) // "cube"
            {
                toolTip.ToolTipButtonToggle(true);
            }
            else if(toolTip.ToolTipTextEnabled == false)
            {
                toolTip.ToolTipButtonToggle(false);
            }
        }
    }

    private void OnEnable()
    {
        //TargetImageToggle(true);            
        //TargetImageToggle(true);            
    }

    #endregion 

    #region Public Methods
    public void TargetImageToggle(bool value)
    {
        targetImage.SetActive(value);
    }

    /// <summary>
    /// Select Game Object and return it.
    /// </summary>
    /// <returns></returns>
    public GameObject SelectObject()
    {
        Ray ray = Camera.main.ScreenPointToRay(targetImage.transform.position);
        RaycastHit hit;
        GameObject obj = null;

        foreach (GameObject spawnObj in SpawnObject.SpawnObjectList)
        {
            // hit.collider.gameObject;
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == spawnObj.tag) //ray && toolTip.ToolTipTextEnabled == true
            {
                obj = hit.collider.gameObject;
                if(obj.GetComponent<MeshCollider>() == null)
                {
                    // obj.AddComponent<MeshCollider>();
                }
            }
        }
        return obj;

    }

    /// <summary>
    /// Object Selection using the Finger Pointer
    /// </summary>
    public void FingerPointerObjectSelection()
    {

        foreach (var obj in SpawnObject.SpawnObjectList)
        {
            // obj.SetActive(false);
        }
        ObjectSelected = true;
    }



    #endregion


}
