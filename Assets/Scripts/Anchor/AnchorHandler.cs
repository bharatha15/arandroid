using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorHandler : MonoBehaviour
{
    #region Serialized Private Fields

    [Header("Script")]
    [SerializeField] private ARTapToPlaceObject aRTapToPlaceObject;

    [Header("Other")]
    [SerializeField] private GameObject aRGameObject;

    #endregion

    #region Public Properties
    public bool AnchorEnable
    {
        set; get;
    }

    public GameObject ARGameObject
    {
        get { return aRGameObject; }
    }

    #endregion


    #region Monobehaviour Callbacks
    // Start is called before the first frame update
    void Start()
    {
        AnchorEnable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Button to Tap and Place the Object on the Anchor.
    /// </summary>
    public void ToggleAnchor()
    {
        switch (AnchorEnable)
        {
            case false:
                AnchorEnable = true;
                break;
            case true:
                AnchorEnable = false;
                break;
        }
    }
    #endregion

}
